# My dotfiles

Started with stow (https://alexpearce.me/2016/02/managing-dotfiles-with-stow/), but managing symlinks by hand for better control now.

Recipe to install (make directories as necessary)
-------------------------------------------------

```
ln -s ~/my-dotfiles/i3_config ~/.config/i3/config
ln -s ~/my-dotfiles/i3status_config ~/.config/i3status/config
ln -s ~/my-dotfiles/polybar_config ~/.config/polybar/config

ln -s ~/my-dotfiles/.bashrc ~/.bashrc
ln -s ~/my-dotfiles/.inputrc ~/.inputrc
ln -s ~/my-dotfiles/.emacs ~/.emacs
ln -s ~/my-dotfiles/emacs_init.el ~/.emacs.d/init.el
```
