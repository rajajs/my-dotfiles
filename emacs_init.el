
;; package archives. elpa most official, melpa and marmalade less restrictive, more choice
(package-initialize)
 (setq package-archives
 '(("melpa-stable" . "http://stable.melpa.org/packages/")
   ("melpa" . "http://melpa.org/packages/")
   ;; ("marmalade"   . "http://marmalade-repo.org/packages/")
   
   ("elpa" . "https://elpa.nongnu.org/nongnu")
   ("gnu"         . "http://elpa.gnu.org/packages/")
   ("org"         . "http://orgmode.org/elpa/")
   ("elpy" . "http://jorgenschaefer.github.io/packages/")))

;; first get use package
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(setq use-package-verbose t)
(setq use-package-always-ensure t)
(eval-when-compile
  (require 'use-package ))


;; automatically update packages every 7 days
(use-package auto-package-update
   :ensure t
   :config
   (setq auto-package-update-delete-old-versions t
         auto-package-update-interval 7)
   (auto-package-update-maybe))


;; Save sessions with desktop 
(desktop-save-mode 1)

;; Looks
(menu-bar-mode 1)
(tool-bar-mode -1)
;;(scroll-bar-mode -1) 
;;(blink-cursor-mode -1)

;;Font
(set-default-font "Hack-10")
(set-default-font "Inconsolata-12")


;; enable y/n answers
(fset 'yes-or-no-p 'y-or-n-p)

;; highlight the current line
(global-hl-line-mode +1)

;; Drive out the mouse when it's too near to the cursor.
(mouse-avoidance-mode 'animate)

(setq frame-title-format
  '("Emacs - " (buffer-file-name "%f"
    (dired-directory dired-directory "%b"))))

;; General utilities
(server-start) ;; server available now

;; org-protocol
(require 'org-protocol)

;; autopair
(electric-pair-mode 1)

(show-paren-mode 1)

;; auto revert
(global-auto-revert-mode 1)

;; ido
(use-package ido
  :ensure t
  :config
  (setq ido-enable-prefix nil
	ido-enable-flex-matching t
	ido-everywhere t)
  (ido-mode 1)
  )

(use-package smex
  :ensure t
  :bind ("M-x" . smex))

;; Move where I mean
(use-package mwim
  :ensure t
  :bind (
	 ("C-a" . mwim-beginning-of-code-or-line)
	 ("C-e" . mwim-end-of-code-or-line)
	 ))


(use-package multiple-cursors
  :ensure t
  :bind (
	 ("C-S-c C-S-c" . mc/edit-lines)
	 ("C->" . mc/mark-next-like-this)
	 ("C-<" . mc/mark-previous-like-this)
	 ("C-c C-<" . mc/mark-all-like-this)
	 ))


;; Line numbers only in certain modes (now python)
(defun my-python-mode-hook () 
  (linum-mode 1)) 
(add-hook 'python-mode-hook 'my-python-mode-hook) 

;; smart-mode-line
(use-package smart-mode-line
  :ensure smart-mode-line
  :init 
  (progn
    (setq sml/no-confirm-load-theme t)
    (sml/setup)
    ;;(sml/apply-theme 'powerline)
    ))

;; zenburn theme
(use-package zenburn-theme
  :ensure t
  :config
(load-theme 'zenburn t))

;; Oct 2021 - trying one-dark (https://github.com/balajisivaraman/emacs-one-themes)
(use-package one-themes
  :init
  (load-theme 'one-dark t))

;; Avoid all font-size changes
(setq emacs-one-height-minus-1 1.0)
(setq emacs-one-height-plus-1 1.0)
(setq emacs-one-height-plus-2 1.0)
(setq emacs-one-height-plus-3 1.0)
(setq emacs-one-height-plus-4 1.0)

;; elpy for python
(use-package elpy
  :ensure t
  :config
  (elpy-enable))


;; web-mode to edit html files
(use-package web-mode
  :ensure t
  :config
  (add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))
  )


;; utf-8 everywhere
(prefer-coding-system 'utf-8)
(set-default-coding-systems 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
;; backwards compatibility as default-buffer-file-coding-system
;; is deprecated in 23.2.
(if (boundp 'buffer-file-coding-system)
    (setq-default buffer-file-coding-system 'utf-8)
  (setq default-buffer-file-coding-system 'utf-8))
;; Treat clipboard input as UTF-8 string first; compound text next, etc.
(setq x-select-request-type '(UTF8_STRING COMPOUND_TEXT TEXT STRING))

;; no backups
(setq backup-inhibited t)

;; true knight
(fset 'yes-or-no-p 'y-or-n-p)

;; reveal location
(use-package ox-reveal
  :ensure t)
(setq org-reveal-root "file:///home/raja/bin/reveal.js-2021") 

;; winner mode
(winner-mode 1)

;; switch buffers
(global-set-key (kbd "<C-tab>") 'ido-switch-buffer)


;; get current word into search
;; http://sachachua.com/blog/2008/07/emacs-keyboard-shortcuts-for-navigating-code/
(defun sacha/isearch-yank-current-word ()
  "Pull current word from buffer into search string."
  (interactive)
  (save-excursion
    (skip-syntax-backward "w_")
    (isearch-yank-internal
     (lambda ()
       (skip-syntax-forward "w_")
       (point)))))
(define-key isearch-mode-map (kbd "C-x") 'sacha/isearch-yank-current-word)


;; --------------
(defun xah-toggle-letter-case ()
  "Toggle the letter case of current word or text selection.
Always cycle in this order: Init Caps, ALL CAPS, all lower.

URL `http://ergoemacs.org/emacs/modernization_upcase-word.html'
Version 2017-04-19"
  (interactive)
  (let (
        (deactivate-mark nil)
        $p1 $p2)
    (if (use-region-p)
        (setq $p1 (region-beginning)
              $p2 (region-end))
      (save-excursion
        (skip-chars-backward "[:alnum:]-_")
        (setq $p1 (point))
        (skip-chars-forward "[:alnum:]-_")
        (setq $p2 (point))))
    (when (not (eq last-command this-command))
      (put this-command 'state 0))
    (cond
     ((equal 0 (get this-command 'state))
      (upcase-initials-region $p1 $p2)
      (put this-command 'state 1))
     ((equal 1  (get this-command 'state))
      (upcase-region $p1 $p2)
      (put this-command 'state 2))
     ((equal 2 (get this-command 'state))
      (downcase-region $p1 $p2)
      (put this-command 'state 0)))))
(global-set-key (kbd "M-c") 'xah-toggle-letter-case)

;; ;; https://emacs.stackexchange.com/questions/2347/kill-or-copy-current-line-with-minimal-keystrokes
;; (defun slick-cut (beg end)
;;   (interactive
;;    (if mark-active
;;        (list (region-beginning) (region-end))
;;      (list (line-beginning-position) (line-beginning-position 2)))))

;; (advice-add 'kill-region :before #'slick-cut)

;; (defun slick-copy (beg end)
;;   (interactive
;;    (if mark-active
;;        (list (region-beginning) (region-end))
;;      (message "Copied line")
;;      (list (line-beginning-position) (line-beginning-position 2)))))

;; (advice-add 'kill-ring-save :before #'slick-copy)


;; Setting up org-ref https://www.reddit.com/r/emacs/comments/4gudyw/help_me_with_my_orgmode_workflow_for_notetaking/
;; (setq org-ref-notes-directory "/tmp/org/references/notes"
;;       org-ref-bibliography-notes "/tmp/org/references/articles.org"
;;       org-ref-default-bibliography '("/tmp/org/references/articles.bib")
;;       org-ref-pdf-directory "/tmp/org/references/pdfs/")

;;(require 'org-ref)			


;; ace mode to change windows
(use-package ace-window)
(global-set-key (kbd "M-o") 'ace-window)	   


;; reclaim M-n combinations (http://pragmaticemacs.com/emacs/use-your-digits-and-a-personal-key-map-for-super-shortcuts/)
(dotimes (n 10)
  ;;(global-unset-key (kbd (format "C-%d" n)))
  (global-unset-key (kbd (format "M-%d" n)))
  )


;; Eyebrowse set up (http://pragmaticemacs.com/emacs/easily-manage-emacs-workspaces-with-eyebrowse/)
(use-package eyebrowse
  :diminish eyebrowse-mode
  :config (progn
            (define-key eyebrowse-mode-map (kbd "M-1") 'eyebrowse-switch-to-window-config-1)
            (define-key eyebrowse-mode-map (kbd "M-2") 'eyebrowse-switch-to-window-config-2)
            (define-key eyebrowse-mode-map (kbd "M-3") 'eyebrowse-switch-to-window-config-3)
            (define-key eyebrowse-mode-map (kbd "M-4") 'eyebrowse-switch-to-window-config-4)
	    (define-key eyebrowse-mode-map (kbd "M-5") 'eyebrowse-switch-to-window-config-5)
	    (define-key eyebrowse-mode-map (kbd "M-6") 'eyebrowse-switch-to-window-config-6)
	    (define-key eyebrowse-mode-map (kbd "M-7") 'eyebrowse-switch-to-window-config-7)
	    (define-key eyebrowse-mode-map (kbd "M-8") 'eyebrowse-switch-to-window-config-8)
            (eyebrowse-mode t)
            (setq eyebrowse-new-workspace t)))


;; Needed now to use org templates
(require 'org-tempo)

;; Add new template for reference for ox-reveal 
(eval-after-load 'org
  '(progn
    (add-to-list 'org-structure-template-alist
                 '("r" . "reference")
		 '("n" . "notes")
		 )
    ))


;; use global mark only from cua (http://www.gnu.org/software/emacs/manual/html_node/emacs/CUA-Bindings.html)
(use-package cua-base
  :if (version<= "24.4" emacs-version)
  :init (progn
          (cua-selection-mode 1)
          (delete-selection-mode 0)
          (setq cua-global-mark-keep-visible nil)))

;; remove highlight indentation from elpy-modules
(delete `elpy-module-highlight-indentation elpy-modules)

;; super save - https://github.com/bbatsov/super-save/
(use-package super-save
  :ensure t
  :config
  (super-save-mode +1)
  (setq auto-save-default nil)
  (setq super-save-remote-files nil)
  (add-to-list 'super-save-triggers 'ace-window)  
  )

;; projectile for project management
(use-package projectile
  :ensure t
  :config
  (projectile-mode +1)
  (define-key projectile-mode-map (kbd "s-p") 'projectile-command-map)
  (define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)  
  )

;; refile to second level headings
(setq org-refile-targets '((nil . (:level . 2))))

;; Dont let org agenda break up window config
(setq org-agenda-window-setup 'current-window) 

;; F10 to export reveal
(global-set-key (kbd "<f10>") 'org-reveal-export-to-html)

;; org-roam (from https://lucidmanager.org/productivity/taking-notes-with-emacs-org-mode-and-org-roam/)
;; Org-Roam basic configuration
(setq org-directory "/data/Dropbox/personal/docs/org-roam/")

(use-package org-roam
  :after org
  :init (setq org-roam-v2-ack t) ;; Acknowledge V2 upgrade
  :custom
  (org-roam-directory (file-truename org-directory))
  :config
  (org-roam-setup)
  :bind (("C-c n f" . org-roam-node-find)
	 ("C-c n g" . org-roam-graph)
	 ("C-c n r" . org-roam-node-random)		    
           (:map org-mode-map
                 (("C-c n i" . org-roam-node-insert)
                  ("C-c n o" . org-id-get-create)
                  ("C-c n t" . org-roam-tag-add)
                  ("C-c n a" . org-roam-alias-add)
                  ("C-c n l" . org-roam-buffer-toggle)))))

;; automatically close buffers that are older than 14 days
(require 'midnight)
(setq clean-buffer-list-delay-general 14)

;; caoture templates
;; specifically for bookmarks
(setq org-capture-templates
      '(
	;; many more capture templates
	("b" "Bookmark" entry (file+headline "~/share/all/org-mode/notes.org" "Bookmarks")
	 "* %?\n:PROPERTIES:\n:CREATED: %U\n:END:\n\n" :empty-lines 1)
	;; many more capture templates
	)
      )	  

;; elpy freezes - not finding python ?
setq elpy-rpc-python-command "python3"

